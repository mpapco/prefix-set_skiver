class LoaderInventory:
    def __init__(self, inventory_dic):
        self.inventory = inventory_dic

    def print_inventory(self):
        selected = None
        for idx, dev in enumerate(self.inventory, start=1):
            print(f"{idx}: {dev['device_name']}")
            # print(f"device: {dev}")
        print(50 * '-')
        try:
            selected = (int(input('Select one of devices available in inventory: '))) - 1
            if selected < 0:
                raise ValueError
            # print(f'Selected device {self.inventory[selected]}')
            return self.inventory[selected]
        except (ValueError, IndexError):
            print(f'Selected: {selected} device is not in inventory. Exiting.')
            exit(1)

    def find_device(self, device):
        for dev in self.inventory:
            # print(f'Device: {dev}')
            if device == dev['device_name']:
                return dev
        print(f'Device: {device} not found in inventory!\n')
        self.print_inventory()




