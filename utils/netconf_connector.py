from ncclient import manager
import xml.dom.minidom
from getpass import getpass


class NetconfConnector:
    def __init__(self, operation, template_rendered):
        self.operation = operation
        self.template = template_rendered
        # print(f'NetconfConnector Operation: {self.operation}')
        # print(f'NetconfConnector Template: {self.template}')

    @staticmethod
    def open_session(device):
        print(f"Opening NETCONF Connection to {device['address']}")
        # Open connection to device with ncclient
        session = manager.connect(
                host=device['address'],
                port=device['netconf_port'],
                username=device['username'],
                password=device['password'],
                # password=getpass('Enter device password: '),
                device_params=device['device_params'],
                hostkey_verify=False,
        )
        # print(f'Session: {session}')
        return session

    @staticmethod
    def close_session(session):
        print(f"Closing NETCONF Connection {session}")
        session.close_session()

    def apply_operation(self, session):
        if self.operation == 'get':
            print('Sending a <get-config> operation to the device.')
            # Make a NETCONF <get-config> query using the filter
            netconf_reply = session.get_config(source='running', filter=self.template)
            print("Here is the raw XML data returned from the device.\n")
            # Print out the raw XML that returned
            print(50 * '-')
            print(netconf_reply)
            # print((xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml()))
            print(50 * '-')
            return netconf_reply

        elif self.operation == 'set':
            # TODO lock_datastore(session)
            print('Locking running datastore.')
            netconf_lockrun_reply = session.lock(target='running')
            print(netconf_lockrun_reply)
            # print(xml.dom.minidom.parseString(netconf_lockrun_reply.xml).toprettyxml())
            print('Locking candidate datastore.')
            netconf_lockcandidate_reply = session.lock(target='candidate')
            print(netconf_lockcandidate_reply)
            # print(xml.dom.minidom.parseString(netconf_lockcandidate_reply.xml).toprettyxml())

            print('Sending edit operation to device.')
            netconf_set_reply = session.edit_config(self.template, target='candidate')
            print(netconf_set_reply)
            # print(xml.dom.minidom.parseString(netconf_set_reply.xml).toprettyxml())

            print('Sending RPC commit operation to device.')
            netconf_save_reply = session.commit()
            print(netconf_save_reply)
            # print(xml.dom.minidom.parseString(netconf_save_reply.xml).toprettyxml())

            # TODO unlock_datastore(session)
            print('Unlocking candidate datastore.')
            netconf_unlockcandidate_reply = session.unlock(target='candidate')
            print(netconf_unlockcandidate_reply)
            # print(xml.dom.minidom.parseString(netconf_unlockcandidate_reply.xml).toprettyxml())
            print('Unlocking running datastore.')
            netconf_unlockrun_reply = session.unlock(target='running')
            print(netconf_unlockrun_reply)
            # print(xml.dom.minidom.parseString(netconf_unlockrun_reply.xml).toprettyxml())

        elif self.operation == 'delete':
            # TODO lock_datastore(session)
            print('Locking running datastore.')
            netconf_lockrun_reply = session.lock(target='running')
            print(netconf_lockrun_reply)
            # print(xml.dom.minidom.parseString(netconf_lockrun_reply.xml).toprettyxml())
            print('Locking candidate datastore.')
            netconf_lockcandidate_reply = session.lock(target='candidate')
            print(netconf_lockcandidate_reply)
            # print(xml.dom.minidom.parseString(netconf_lockcandidate_reply.xml).toprettyxml())

            print('Sending delete operation to device.')
            netconf_delete_reply = session.edit_config(self.template, target='candidate')
            print(netconf_delete_reply)
            # print(xml.dom.minidom.parseString(netconf_delete_reply.xml).toprettyxml())

            print('Sending RPC commit operation to device.')
            # netconf_save_reply = m.dispatch(xml_.to_ele(save_body))
            netconf_save_reply = session.commit()
            print(netconf_save_reply)
            # print(xml.dom.minidom.parseString(netconf_save_reply.xml).toprettyxml())

            # TODO unlock_datastore(session)
            print('Unlocking candidate datastore.')
            netconf_unlockcandidate_reply = session.unlock(target='candidate')
            print(netconf_unlockcandidate_reply)
            # print(xml.dom.minidom.parseString(netconf_unlockcandidate_reply.xml).toprettyxml())
            print('Unlocking running datastore.')
            netconf_unlockrun_reply = session.unlock(target='running')
            print(netconf_unlockrun_reply)
            # print(xml.dom.minidom.parseString(netconf_unlockrun_reply.xml).toprettyxml())

        else:
            print(f'Unknown operation {self.operation}. Will do nothing.')
