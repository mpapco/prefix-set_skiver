import yaml
import xml.etree.ElementTree as ElementTree
import datetime
import os


class SaverYAML:
    def __init__(self, device, ncreply):
        self.device = device
        self.ncreply = ncreply
        self.saved = self._save_to_file()

    def _convert_ncreply_to_yaml(self):
        root = ElementTree.fromstring(str(self.ncreply))
        # print(f'ET: {root.tag} {root.attrib} {root.text}')

        prefix_sets = {'prefix_sets': []}
        """ Format
        { 'prefix_sets':
            [
                {
                    'name': 'PS_TEST1', 
                    'prefixes': [
                        {'prefix': '1.1.1.0/24', 'mask': '27..30'}, 
                        {'prefix': '2.2.2.0/26', 'mask': '27..32'}
                    ]
                },  
                {
                    'name': 'PS_TEST4', 
                    'prefixes': [
                        {'prefix': '1.1.1.0/24', 'mask': '25..30'}, 
                        {'prefix': '2.2.2.0/25', 'mask': '26..32'}, 
                        {'prefix': '1.1.1.0/27', 'mask': '27..32'}, 
                        {'prefix': '2.2.2.0/28', 'mask': '29..32'}
                    ]
                }, 
                {
                    'name': 'PS_TEST6'
                }
            ]
        }
        """

        namespace = {'ns': 'http://openconfig.net/yang/routing-policy'}
        # Check if the prefix-set is empty
        try:
            for prefix_set in root[0][0][0][0]:
                # <prefix-set>
                prefix_set_dic = {}
                for prefix_set_name in prefix_set.findall('ns:prefix-set-name', namespace):
                    # print(f'PS name: {prefix_set_name.text}')
                    prefix_set_dic['name'] = prefix_set_name.text
                    for prefixes in prefix_set.findall('ns:prefixes', namespace):
                        prefixes_list = []
                        for prefix in prefixes:
                            prefix_dic = {}
                            for ip_prefix in prefix.findall('ns:ip-prefix', namespace):
                                # print(f'\tIP Prefix: {ip_prefix.text}')
                                prefix_dic['prefix'] = ip_prefix.text
                            for mask in prefix.findall('ns:masklength-range', namespace):
                                # print(f'\tMask: {mask.text}')
                                prefix_dic['mask'] = mask.text
                            prefixes_list.append(prefix_dic)
                        prefix_set_dic['prefixes'] = prefixes_list
                # print(f'PS_DIC: {prefix_set_dic}')
                prefix_sets['prefix_sets'].append(prefix_set_dic)

            # print(f'PREFIX_SET: \n{prefix_sets}')

            return prefix_sets
        except IndexError:
            return False

    def _save_to_file(self):
        # print(f"Devicename: {self.device['device_name']}")
        # print(f'Current datetime: {datetime.datetime.now().strftime("%Y%m%d_%H:%M:%S")}')
        filename = self.device['device_name'] + '_' + datetime.datetime.now().strftime("%Y%m%d_%H%M%S") + '.yaml'
        # print(f'Filename: {filename}')

        # check if /data directory exists, if not then create it
        if not os.path.isdir('./data'):
            os.mkdir('data')

        with open('data/' + filename, 'w') as output:
            if self._convert_ncreply_to_yaml():
                prefix_set_in_yaml = self._convert_ncreply_to_yaml()
                yaml.dump(prefix_set_in_yaml, output, indent=2, sort_keys=False)
            else:
                print(50 * '!')
                print('There is no prefix-set configured that matches criteria.')
                print(50 * '!')

        return filename

    def get_filename(self):
        return self.saved






