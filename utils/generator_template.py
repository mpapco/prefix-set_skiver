from jinja2 import Environment, FileSystemLoader


class LoaderJ2:
    def __init__(self):
        # Set the environment to './templates'
        # This means that any file names used are in the /templates directory the script is located in
        self.ENV = Environment(loader=FileSystemLoader('./templates'))

    def _load_template(self, operation):
        template = None
        if operation == 'getall':
            template = self.ENV.get_template('getall_prefixset.j2')
        elif operation == 'get':
            template = self.ENV.get_template('get_prefixset.j2')
        elif operation == 'set':
            template = self.ENV.get_template('set_prefixset.j2')
        elif operation == 'delete':
            template = self.ENV.get_template('del_prefixset.j2')
        else:
            print('Unknown operation!')
            exit(1)
        return template

    def generate_template(self, operation=None, prefixset=None):
        # print(f'Generate {operation} template for prefixes: {prefixset}')
        template = self._load_template(operation)
        return template.render(prefixset)


