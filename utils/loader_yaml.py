import yaml


class LoaderYAML:
    def __init__(self, in_file):
        self.in_dic = self._yaml2dic(in_file)

    @staticmethod
    def _yaml2dic(in_file):
        return yaml.load(open(in_file), Loader=yaml.FullLoader)

    def dicfromyaml(self):
        return self.in_dic
