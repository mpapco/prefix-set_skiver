#!/usr/bin/env python
import click
import click_completion

from utils import loader_yaml
from utils import generator_template
from utils import netconf_connector
from utils import saver_yaml
from utils import loader_inventory

click_completion.init()


@click.group()
def main():
    """
    Set, Get or Delete prefix sets configured on Cisco device running IOS-XR.
    Tested on IOS-XR 6.5.3.
    Input is prefix-sets file in YAML format. If no inventory file is specified, by default the one
    saved in config/device_inventory.yaml us used.

    \b
    Prefix set file is mandatory parameter (except for getall).
    Output from get methods is saved in 'data' directory.

    \b
    used YANG models:
    - urn:ietf:params:xml:ns:netconf:base:1.0
    - http://openconfig.net/yang/routing-policy

    \b
    Examples:
        $ run.py getall
        $ run.py getall -d Router_A
        $ run.py get -ps prefix-set_file.yaml
        $ run.py [get|set|del] -ps prefix-set_file.yaml -i inventory.yaml
        $ run.py [get|set|del] -ps prefix-set_file.yaml -i inventory.yaml -d Router_A
        $ run.py [get|set|del] -ps prefix-set_file.yaml
    """


@click.option('--dev', '-d', 'device',
              type=str,
              default=None,
              show_default=True,
              help='Set device that will be configured (must be in inventory).')
@click.option('--inventory', '-i', 'inventory_file',
              type=click.Path(exists='True'),
              default='config/device_inventory.yaml',
              show_default=True,
              help='Set file that contains prefix set definitions.'
              )
@main.command(name='getall')
def getall_prefix_set(device, inventory_file):
    """
    Obtain all prefix sets configured on device/all devices in inventory.
    """
    # Load prefix and inventory
    inventory_dic = loader_yaml.LoaderYAML(inventory_file).dicfromyaml()
    # print(f'Loaded Inventory: {inventory_dic}')
    prefixset_dic = []

    # Generate template
    template_obj = generator_template.LoaderJ2()
    template_rendered = template_obj.generate_template(operation='getall', prefixset=prefixset_dic)

    print(50 * '-')
    print(f"Generated template: \n{template_rendered}")
    print(50 * '-')

    # Set NETCONF session variables (operation, template)
    connector = netconf_connector.NetconfConnector('get', template_rendered)
    # Loop through each device in inventory

    dev_inventory = loader_inventory.LoaderInventory(inventory_dic)
    # Device entered via CLI or choosed by user from printed list
    if device is not None:
        device = dev_inventory.find_device(device)
        # Open session to device
        session = netconf_connector.NetconfConnector.open_session(device)
        # Send request to device
        ncreply = connector.apply_operation(session)
        # Save loaded prefix sets to YAML file
        saver = saver_yaml.SaverYAML(device, ncreply)
        print(f'Output saved to {saver.get_filename()}')

        # Close session to device
        netconf_connector.NetconfConnector.close_session(session)
    else:
        for device in inventory_dic:
            # Open session to device
            session = netconf_connector.NetconfConnector.open_session(device)
            # Send request to device
            ncreply = connector.apply_operation(session)
            # Save loaded prefix sets to YAML file
            saver = saver_yaml.SaverYAML(device, ncreply)
            print(f'Output saved to {saver.get_filename()}')

            # Close session to device
            netconf_connector.NetconfConnector.close_session(session)


@click.option('--dev', '-d', 'device',
              type=str,
              default=None,
              show_default=True,
              help='Set device that will be configured (must be in inventory).')
@click.option('--prefix-set', '-ps', 'prefixset_file',
              type=click.Path(exists='True'),
              required=True,
              help='Set file that contains prefix set definitions.')
@click.option('--inventory', '-i', 'inventory_file',
              type=click.Path(exists='True'),
              default='config/device_inventory.yaml',
              show_default=True,
              help='Set file that contains prefix set definitions.'
              )
@main.command(name='get')
def get_prefix_set(device, prefixset_file, inventory_file):
    """
    Obtain prefix sets configured on device.
    """
    # Load prefix and inventory
    inventory_dic = loader_yaml.LoaderYAML(inventory_file).dicfromyaml()
    # print(f'Loaded Inventory: {inventory_dic}')
    prefixset_dic = loader_yaml.LoaderYAML(prefixset_file).dicfromyaml()
    # print(f'Loaded prefixset: {prefixset_dic}')

    # Generate template
    template_obj = generator_template.LoaderJ2()
    template_rendered = template_obj.generate_template(operation='get', prefixset=prefixset_dic)

    print(50 * '-')
    print(f"Generated template: \n{template_rendered}")
    print(50 * '-')

    # Set NETCONF session variables (operation, template)
    connector = netconf_connector.NetconfConnector('get', template_rendered)

    dev_inventory = loader_inventory.LoaderInventory(inventory_dic)
    # Device entered via CLI or choosed by user from printed list
    if device is None:
        device = dev_inventory.print_inventory()
    else:
        device = dev_inventory.find_device(device)

    # TODO method netconf_operation
    # TODO netconf_connector.OperationHandler.handle('get', connector)
    # print(f'Device: {device}')
    # Open session to device
    session = netconf_connector.NetconfConnector.open_session(device)
    # Send request to device
    ncreply = connector.apply_operation(session)
    # Save loaded prefix sets to YAML file
    saver = saver_yaml.SaverYAML(device, ncreply)
    print(f'Output saved to {saver.get_filename()}')

    # Close session to device
    netconf_connector.NetconfConnector.close_session(session)


@click.option('--dev', '-d', 'device',
              type=str,
              default=None,
              show_default=True,
              help='Set device that will be configured (must be in inventory).')
@click.option('--prefix-set', '-ps', 'prefixset_file',
              type=click.Path(exists='True'),
              required=True,
              help='Set file that contains prefix set definitions.')
@click.option('--inventory', '-i', 'inventory_file',
              type=click.Path(exists='True'),
              default='config/device_inventory.yaml',
              show_default=True,
              help='Set file that contains prefix set definitions.'
              )
@main.command(name='set')
def set_prefix_set(device, prefixset_file, inventory_file):
    """
    Set prefix sets configured on device.
    """
    # Load prefix and inventory
    inventory_dic = loader_yaml.LoaderYAML(inventory_file).dicfromyaml()
    # print(f'Loaded Inventory: {inventory_dic}')
    prefixset_dic = loader_yaml.LoaderYAML(prefixset_file).dicfromyaml()
    # print(f'Loaded prefixset: {prefixset_dic}')

    # Generate template
    template_obj = generator_template.LoaderJ2()
    template_rendered = template_obj.generate_template(operation='set', prefixset=prefixset_dic)

    print(50 * '-')
    print(f"Generated template: \n{template_rendered}")
    print(50 * '-')

    confirmation = (input('Do you want to apply generated template to device (y/n)?\n').lower())

    if confirmation == '' or confirmation == 'n':
        print('No changes will be applied.')
        exit(1)
    elif confirmation == 'y':
        # Set NETCONF session variables
        connector = netconf_connector.NetconfConnector('set', template_rendered)

        dev_inventory = loader_inventory.LoaderInventory(inventory_dic)
        # Device entered via CLI or choosed by user from printed list
        if device is None:
            device = dev_inventory.print_inventory()
        else:
            device = dev_inventory.find_device(device)

        # TODO method netconf_operation
        # TODO netconf_connector.OperationHandler.handle('set', connector)
        # print(f'Device: {device}')
        # Open session to device
        session = netconf_connector.NetconfConnector.open_session(device)
        # Send request to device
        connector.apply_operation(session)
        # Close session to device
        netconf_connector.NetconfConnector.close_session(session)
    else:
        print(f'Unknown confirmation {confirmation}. No changes will be applied.')
        exit(1)


@click.option('--dev', '-d', 'device',
              type=str,
              default=None,
              show_default=True,
              help='Set device that will be configured (must be in inventory).')
@click.option('--prefix-set', '-ps', 'prefixset_file',
              type=click.Path(exists='True'),
              required=True,
              help='Set file that contains prefix set definitions.')
@click.option('--inventory', '-i', 'inventory_file',
              type=click.Path(exists='True'),
              default='config/device_inventory.yaml',
              show_default=True,
              help='Set file that contains prefix set definitions.'
              )
@main.command(name='del')
def delete_prefix_set(device, prefixset_file, inventory_file):
    """
    Delete prefix sets configured on device.
    """
    # Load prefix and inventory
    inventory_dic = loader_yaml.LoaderYAML(inventory_file).dicfromyaml()
    # print(f'Loaded Inventory: {inventory_dic}')
    prefixset_dic = loader_yaml.LoaderYAML(prefixset_file).dicfromyaml()
    # print(f'Loaded prefixset: {prefixset_dic}')

    # Generate template
    template_obj = generator_template.LoaderJ2()
    template_rendered = template_obj.generate_template(operation='delete', prefixset=prefixset_dic)
    print(50 * '-')
    print(f"Generated template: \n{template_rendered}")
    print(50 * '-')

    confirmation = (input('Do you want to apply generated template to device (y/n)?\n').lower())

    if confirmation == '' or confirmation == 'n':
        print('No changes will be applied.')
        exit(1)
    elif confirmation == 'y':
        # Set NETCONF session variables
        connector = netconf_connector.NetconfConnector('delete', template_rendered)

        dev_inventory = loader_inventory.LoaderInventory(inventory_dic)
        # Device entered via CLI or choosed by user from printed list
        if device is None:
            device = dev_inventory.print_inventory()
        else:
            device = dev_inventory.find_device(device)

        # TODO method netconf_operation
        # TODO netconf_connector.OperationHandler.handle('get', connector)
        # print(f'Device: {device}')
        # Open session to device
        session = netconf_connector.NetconfConnector.open_session(device)
        # Send request to device
        connector.apply_operation(session)
        # Close session to device
        netconf_connector.NetconfConnector.close_session(session)
    else:
        print(f'Unknown confirmation {confirmation}. No changes will be applied.')
        exit(1)


if __name__ == "__main__":
    main()

# TODO clean output from rpc-replies (OK, Error instead of complete RPC message)
