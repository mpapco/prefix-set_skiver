# Automation of adding and deleting prefix-sets on Cisco IOS-XR with NETCONF protocol

- The prefix sets to be processed are saved in YAML file.
- Methods for adding/getting/deleting preix sets load YAML and perform actions.
- User edits YAML file and runs specific method to achive desired configuration change on the target device.
- List of available target devices is loaded also from YANG file.
